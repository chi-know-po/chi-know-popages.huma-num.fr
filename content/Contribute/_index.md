---
title: "Contribute"
date: 2024-08-27
description: ""
keywords: []
weight: 7
---

If you want to update the documentation and you have contributor acess, you can directly contribute on the Gitlab project by editing the markdown of the page you want to modify.

However, if you want to make bigger changes (e.g., modify structure, adding new sections) it is recommended to modify the repository locally.  
To do so, you will need to take the following steps:

1. Clone the repository :  
   `git clone https://gitlab.huma-num.fr/chi-know-po/chi-know-po.gitpages.huma-num.fr.git`
2. Configure the development environment with the necessary packages by following the instructions on README.md.
3. Make the changes to the markdown pages in the `content` folder, or create new pages by following the models:  
   - If you need to create a new section, make a new directory and create an `_index.md` file under it. To add other pages to this section, simply create new files with the page name as its filename (e.g., for "Corpora" under "Library" section we will have `Library/_index.md` and `Library/Corpora.md`)
   - Every page (including `_index.md`) needs to have a header in the following format:
        ```markdown
        ---
        title: "Corpora"
        date: 20xx-xx-xx
        description: ""
        keywords: []
        author: "John Doe"
        ---
        ```
        **/!\\** The date and the author need to be updated after edition.  
4. You can test the changes you made by testing the website locally. To do so, edit `config.toml` by commenting the first line and uncommenting the second:  

    *Production:*
    ```markdown
    baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
    # baseURL = "http://localhost:1313/" # for local testing
    ```
    *Testing*
    ```markdown
    # baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
    baseURL = "http://localhost:1313/" # for local testing
    ```

    Then, run the following command in your terminal:
    ```bash
    hugo server
    ```
    Now you can view the website locally by accessing [http://localhost:1313/](http://localhost:1313/) and any change you make will be displayed.  
    **/!\\** When you are done testing, **do not push `config.toml`** with the testing URL parameters.

5. Now, you can push your changes and wait for the CI/CD deployment steps before seeing the result on [the website](https://chi-know-po.gitpages.huma-num.fr/).
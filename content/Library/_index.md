---
title: "Library"
date: 2024-08-22
description: ""
keywords: []
weight: 1
---

Part of the CHI-KNOW-PO project includes the acquisition of a large body of texts that will be used for analysis.

The reasons for this digitization are the following:
- Many of the digital texts available online are provided under a limitative license;
- Open source texts are not systematically structured (which proves problematic as soon as we want to analyze works made of multiple layers of texts such as anthologies, encyclopedias or texts associated with exegetical apparatus);
- There are still many works that are not available in full text due to their rather marginal status.
 
Hence, the CHI-KNOW-PO team has started to work hand in hand with four libraries in France to digitalize some of their books [^1]. Digitization will start as of January 2023. Then, the CHI-KNOW-PO team will work with the CALFA team specialized in HTR (handwritten text recognition) in order to acquire the books in full text[^2].

The texts are being edited in XML-TEI as a means to structure them and conform to standards. They will be available under a Creative Commons license for free reuse. Training models and samples for HTR will also be made freely accessible and reusable.

This part of the project is funded by COLLEX [^3].

The edited texts are progressively archived in the Nakala repository to facilitate further reuse.

An online website is under construction to allow consultation, queries, and filtering. Link to the website will be provided in the fall of 2024.

For further details, please see:

- [Corpora]({{< ref "Corpora.md" >}} "Corpora")
- [Text acquisition]({{< ref "Text_Acquisition.md" >}} "Text acquisition")
- [Metadata]({{< ref "Metadata.md" >}} "Metadata")
- [Text Edition]({{< ref "Text_Edition.md" >}} "Text Edition")
- [To Repository]({{< ref "To_Repository.md" >}} "To Repository")

You may directly access this part of the project [on GitLab](https://gitlab.huma-num.fr/chi-know-po/library)

## References

[^1]: Namely: Bibliothèque universitaire des langues et civilisations (BULAC), Bibliothèque de l'Institut d'études chinoises du Collège de France (IHEC), Bibliothèque nationale universitaire de Strasbourg (BNU), Bibliothèque de l'institut d'études chinoises de l'Université de Strasbourg.

[^2]: For more information on Calfa, see their [website](https://calfa.fr/) and their [annotation platform](https://vision.calfa.fr/).

[^3]: COLLEX is a French National Funding meant to support projects related to libraries and archives, see our project on their [website](https://www.collexpersee.eu/projet/chi-know-po-corpus/).
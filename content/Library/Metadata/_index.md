---
title: "Metadata"
date: 2024-11-25
description: ""
keywords: []
weight: 1
---

The CHI-KNOW-PO project includes a very large set of files in different format.

It is essential that all these files are properly documented in order to ensure reusibility in a scientific context and beyond.

Because the CHI-KNOW-PO project is hosted in a French institution, because it deals with sources in Chinese, and because it is meant to be accessible to colleagues at a global level, metadata are natively plurilingual.

We have found solutions to document each element within metadata in all needed languages despite some constraints in the Nakala repository (the creator element cannot be repeated for example). Also, because the Nakala repository is designed to display authors and titles according to the alphabetical order, it has been decided to choose English (or pinyin transliteration) for language 1, Chinese for language 2, and French for language 3.

The idea behind this tool, which was built using the notebook written by Adrien Desseigne and published by the IR* Huma-Num, is to provide an easy to use set of files to minutely describe files in multiple languages before they are sent to a repository.

The metadata is documented and stored in a `yaml file` which is then used:
- when the text is being edited (XML-TEI file production)
- when the files are being sent to the Nakala repository.

In the metadata-related explanations below, we distinguish between:
- [Categorization tools]({{< ref "Categorization_Tools.md" >}} "Categorization tools")
- [Informations with documentation purposes]({{< ref "Documentation_Information.md" >}} "Documentation information")
- [Data relevant in a digital framework]({{< ref "Digital_Data_Info.md" >}} "Informations of use for digital purposes")
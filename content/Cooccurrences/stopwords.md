---
title: "Stopwords"
date: 2024-08-23
description: ""
keywords: []
weight: 3
---

# Choices
The necessity of using a stopword list and what should constitute one are disputable questions. On one hand, removing some of the high-frequency words, especially the functional ones, helps achieve cleaner results that are easier to interpret. On the other, it is impossible to create a list that would work equally well for texts of different genres and time periods. Moreover, there is no universally acknowledged way of creating such lists and the ones that already exist are quite controversial (for example, see [^1] for a discussion related to stop lists in English). 

This project deals with this issue in different manners depending on the steps of analysis.

## Collocation finder
Since this part of the scripts only looks at usage statistics for a set list of words, no stop word lists are used for counting in this part.

## Pair explorer
Stop words are used when counting the top frequent words in the scope of the search pair – but these are customizable and can be easily changed. 

1. Currently used list is in the scripts/stopwords.py file. It contains 69 words that were manually selected, primarily to work with the Quan Tangshi corpus. The aim was to create a minimally invasive list with as few content words in it as possible. One can, however, choose to adapt the list as they wish. There are two other lists known to us that have been created for Classical Chinese: by Slingerland et. al. [^2] and Allen et. al. [^3], one might choose to reuse them for the sake of research continuity. However, the decisions behind both of them are not well-documented, the first one is very large; the second is not accessible at the moment, so we have chosen not to take them into account.
2. Additionally, stopwords.py the file contains a list of punctuation marks for cleaning the corpora.
3. It is possible to manually add words to the stoplist within the collocations.ipynb file – see settings in the pair_explorer part of the notebook. This is done to give an opportunity to quickly adapt the output results.
4. Ideally, there will be several different lists relevant to the type of corpus analyzed, but this has not been implemented yet. 

## Stopword Lists Rationale

Three lists have been defined so far.

As for the poetry and prose lists, the rationale behind their setting up were the following ones:
- [a.] Most frequent words blur our perception of the occurrence of less frequent words in a large corpus;
- [b.] However, some very frequent words, because they carry non trivial meaning should not be disregarded at the first stages of analysis;
- [c.] Hence, the stopwords list shall focus mainly on grammatical words (_xuci_ 虛詞) with a few exceptions corresponding to generic words (in the realm of our research purposes, _i.e._, most frequent generic words in the semantic categories under study);
- [d.] It should always be possible to add or retrieve a word from the stopwords list for the purpose of a finely designed analysis.

1. The poetry list: This manually compiled list is based on poems from the _Shijing_ 詩經, the _Wenxuan_ 文選 and the _Quan Tang shi_ 全唐詩. Most common words shared in common were identified and added to the relevant grammatical words for poetry. 
2. The prose list: This manually compiled list is based on the Kong Yingda 孔穎達 commentary to the _Shijing_ 詩經. It is mostly constituted of grammatical words.
3. The punctuation list: For comfort of reading, we have decided to provide punctuated texts, both in poetry and prose. As a consequence, we will need to consider Chinese signs of punctuation along with line segmenters as stop words.

## Sources
[^1]: Nothman Joel, Qin Hanmin, and Yurchak Roman. "Stop Word Lists in Free Open-source Software Packages". _Proceedings of Workshop for NLP Open Source Software_. 2018: 7–12.

[^2]: Slingerland Edward , Nichols Ryan, Neilbo Kristoffer and Logan Carson. "The Distant Reading of Religious Texts: A “Big Data” Approach to Mind-Body Concepts in Early China", _Journal of the American Academy of Religion_. Volume 85, Issue 4 (2017): 985–1016.

[^3]: Allen Colin, Luo Hongliang, Murdock Jaimie, Pu Jianghuai, Wang Xiaohong, Zhai Yanjie, and Zhao Kun. “Topic Modeling the Hàn diăn Ancient Classics (汉典古籍)”. _Journal of Cultural Analytics_ Volume 2, Issue 1(2017): online ressource (https://doi.org/10.22148/001c.11882).

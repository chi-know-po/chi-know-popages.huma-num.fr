---
title: "Categorization tools"
date: 2024-11-25
description: ""
keywords: []
weight: 2
---

## Collections
Collections are defined by a title. They correspond to different levels of titles (e.g. *conshu* 叢書 (collection) titles, book titles or section titles). Collections can be subdivided in smaller collections defined by the digital format of the files.

These subdivisions, which are mainly visible in the Nakala repository, and partly stored in the metadata of each file, result in a tree of the following kind:

```
CHI-KNOW-PO
├── Bowuzhi
│   ├── Bowuzhi_vol01
│   ├── ├── Bowuzhi_vol01(images)
│   ├── ├── Bowuzhi_vol01(TEI)
│   ├── Bowuzhi_vol02
│   ├── ├── ...
│   ├── Bowuzhi_vol03
│   ├── ...
├── QuanTangshi
│   ├── QuanTangshi_Taizong
│   ├── ├── QuanTangshi_Taizong(images)
│   ├── ├── QuanTangshi_Taizong(TEI)
│   ├── QuanTangshi_Gaozong
│   ├── ...
````

## Text Categorization

The CHI-KNOW-PO database distinguishes three levels of categorization for writing:
1. Writing type: Corresponds to the genre to which the writing belongs (or is associated)
2. Bibliographical category: One of the six categories defined in bibliographical catalogues in official imperial writings (for a minute description of each category, see Estrades)
3. CKP mode: Of use for the display of the text, three possibilities (prose, poetry, encyclopedia)

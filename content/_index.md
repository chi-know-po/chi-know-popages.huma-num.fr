---
title: "The Chinese Knowledge in Poetry Project"
date: 2024-08-22
layout: "home"
author: "John Doe"
---

## General Introduction

The project, abbreviated here to CHI-KNOW-PO, is hosted by the USIAS (Institute for Advanced Studies of the University of Strasbourg) and led by Marie Bizais-Lillig and her team. It benefits from the COLLEX project entitled CHI-KNOW Corpus. The project also receives assistance from the PHUN Team at the MISHA (Strasbourg).

The team members include Elsa Cuillé, Xinmin Hu, Shueh-Ying Liao, Tilman Schalmey, and Mariana Zorkina.

## Main Goals

The project aims to demonstrate that the textual realm in medieval China (220-907) was continuous and that intertextuality was not specific to poetry. As a consequence, knowledge, along with its textual fragments, circulated through many different genres.

The project focuses on the theme of plants, which hold a central position in Chinese poetic grammar. The plan is to reconstruct the paths that textual fragments concerning plants traveled within a body of texts composed of five parts: the *Shijing* 詩經 [Classic of Poetry] that stood as a point of reference for all literati, successive commentaries on the Classic, elegant texts of the medieval period that refer to these same plants, the first dictionaries, lexicons, and encyclopedias, as well as technical works and treatises.

This project stems from the [Shijing-net](https://gitlab.huma-num.fr/chi-know-po/shijing-net) project which initially involved Marie Bizais-Lillig and Ilaine Wang.

The tools developed and the corpus edited during the project are meant to be reusable in other projects.

## Current Stage Overview

At the current stage (November 2023), the team has worked to identify and visualize cooccurrences of words within a limited corpus (a larger corpus edited in XML-TEI is under preparation).

The identification is based on three elements:
- A corpus: here limited to the *Quan Tang shi* 全唐詩 anthology [The Complete Poems of the Tang] and the *Mao Shi zhengyi* 毛詩正義 anthology [The Righteous Meaning of the Poems in the Mao Tradition].
- A database: the SQL database includes named entities related to the corpus and words pertaining to the realms of plants, natural phenomena, and emotions. These words are embedded in categories and linked together by relation of equivalence (rather than synonymy *per se*).
- Scripts: three main scripts were produced in order to (1) identify and count cooccurrence of two words or two sets of words pertaining to the same category or to the same network of equivalence, (2) visualize a larger set of cooccurrences within the corpus, and (3) track (general or sequential, exact or loose) overlaps between texts.

## Topics of Interest

Explore the various sections of the project:

- [Cooccurrences](https://chi-know-po.gitpages.huma-num.fr/cooccurrences/)
  - [All About Plants](https://chi-know-po.gitpages.huma-num.fr/cooccurrences/all_about_plants/)
  - [Corpus](https://chi-know-po.gitpages.huma-num.fr/cooccurrences/corpus/)
- [External Resources](https://chi-know-po.gitpages.huma-num.fr/external_resources/)
- [Knowledge Base](https://chi-know-po.gitpages.huma-num.fr/knowledge_base/)
  - [Bio-bibliographical Choices](https://chi-know-po.gitpages.huma-num.fr/knowledge_base/bio_bibliographical_choices/)
  - [Construction](https://chi-know-po.gitpages.huma-num.fr/knowledge_base/construction/)
  - [Lexical Choices](https://chi-know-po.gitpages.huma-num.fr/knowledge_base/lexical_choices/)
  - [Sources](https://chi-know-po.gitpages.huma-num.fr/knowledge_base/sources/)
- [Library](https://chi-know-po.gitpages.huma-num.fr/library/)
  - [Corpora](https://chi-know-po.gitpages.huma-num.fr/library/corpora/)
  - [Metadata](https://chi-know-po.gitpages.huma-num.fr/library/metadata/)
  - [Text Edition](https://chi-know-po.gitpages.huma-num.fr/library/text_edition/)
  - [Text Extraction](https://chi-know-po.gitpages.huma-num.fr/library/text_extraction/)
  - [To Repository](https://chi-know-po.gitpages.huma-num.fr/library/to_repository/)
- [Online Tools](https://chi-know-po.gitpages.huma-num.fr/online_tools/)
- [Self-documentation](https://chi-know-po.gitpages.huma-num.fr/self_documentation/)
- [Text Overlap](https://chi-know-po.gitpages.huma-num.fr/text_overlap/)
  - [Corpus](https://chi-know-po.gitpages.huma-num.fr/text_overlap/corpus/)
  - [Shijing-net](https://chi-know-po.gitpages.huma-num.fr/text_overlap/shijing_net/)

## Access to Repositories

- [Shijing-net](https://gitlab.huma-num.fr/chi-know-po/shijing-net): The overlap tracker tool applied to the *Mao Shi zhengyi* 毛詩正義
- [All about plants](https://gitlab.huma-num.fr/chi-know-po/all-about-plants): The collocation finder and pair explorer tools applied to the *Quan Tang shi* 全唐詩
- [Knowledge base](https://gitlab.huma-num.fr/chi-know-po/knowledge-base): The CHI-KNOW-PO database and its settings
- [Library](https://gitlab.huma-num.fr/chi-know-po/library): The CHI-KNOW-PO corpora and text acquisition resources

## Reference DMP

An enriched version of the Data Management Plan produced for the [Shijing-net](https://gitlab.huma-num.fr/chi-know-po/shijing-net) project will be provided here soon (link to come).

## Funding Support

The project benefits from the support of:
- [UR 1340 - GÉO (Université de Strasbourg)](geo.unistra.fr)
- [USIAS (University of Strasbourg Institute for Advanced Studies)](https://www.usias.fr/en/fellows/fellows-2021/marie-bizais-lillig/)
- [COLLEX](https://www.collexpersee.eu/projet/chi-know-po-corpus/)
- [IR* Huma-Num](https://www.huma-num.fr/)

---
title: "Self-documentation"
date: 2024-09-17
description: ""
keywords: []
weight: 6
---

This website was produced through [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) using [Hugo static site generator](https://github.com/gohugoio/hugo). This section aims to decompose the steps we used to generate this site and how to contribute.


Learn more about GitLab Pages at the [official documentation](https://docs.gitlab.com/ce/user/project/pages/).

Before deploying the website to a gitlab page, you should build it locally.  
We will get through how to do configure and test your website, then we will get into the deployment phase with Gitlab Pages and CI/CD.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project.
2. [Install](https://gohugo.io/getting-started/installing/) Hugo.
3. Install the theme as a submodule:
   > `Note: here we used our own fork of Ace-documentation in which we deactivated Google-Analytics as it was causing errors when deploying the site with CI/CD.`

   ```shell
    # add the ace-documentation repository as a submodule in the themes/ directory
    git submodule add https://github.com/frederic-saudemont/ace-documentation.git themes/ace-documentation

    # initialize and update the submodule to fetch its content
    git submodule update --init --recursive

    # add the changes to the .gitmodules file and the submodule directory
    git add .gitmodules themes/ace-documentation

    # commit the changes
    git commit -m "added ace-documentation as a submodule in the themes folder"
   ```

4. Preview your project:

   ```shell
   hugo server
   ```

5. Add content.
6. Optional. Generate the website:

   ```shell
   hugo
   ```

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

## Use a custom theme

Hugo supports a variety of themes.

Visit <https://themes.gohugo.io/> and pick the theme you want to use. For this site, we use [our own fork](https://github.com/frederic-saudemont/ace-documentation) of [Ace documentation](https://github.com/vantagedesign/ace-documentation) in which we deactivated Google-Analytics as it was causing errors when deploying the site with CI/CD. 
 
This is the link to this fork if you want to use it: <https://github.com/frederic-saudemont/ace-documentation>

To use your own theme:

- Edit `config.toml` and add the theme:

   ```plaintext
   theme = "ace-documentation"
   ```

## Local Testing

You can view the changes you made by testing the website locally. To do so, edit `config.toml` by commenting the first line and uncommenting the second:

  *Production:*
  ```markdown
  baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
  # baseURL = "http://localhost:1313/" # for local testing
  ```
  *Testing*
  ```markdown
  # baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
  baseURL = "http://localhost:1313/" # for local testing
  ```

  Then, run the following command in your terminal:
  ```bash
  hugo server
  ```

  Now you can view the website locally by accessing [http://localhost:1313/](http://localhost:1313/) and any change you make will be displayed.  

  **/!\\** When you are done testing, **do not push `config.toml`** with the testing URL parameters.

## Building with GitLab Pages

To build and deploy your Hugo site with GitLab Pages, follow these steps:

### 1. Permissions

To build and deploy the site using GitLab Pages, you need to have the **Maintainer** or **Owner** role in the repository. These roles have the necessary permissions to trigger the pipeline and deploy the site.

If you are a **Developer** you can contribute once the site is deployed.

### 2. GitLab CI/CD Configuration

GitLab Pages is built using GitLab’s CI/CD pipeline. The `.gitlab-ci.yml` file defines how the site will be built and deployed. Below is the correct `.gitlab-ci.yml` configuration for deploying a Hugo site:

```yaml
#
# Before using this .gitlab-ci.yml:
#
# - This example uses the latest Docker image, but you might want to use the
#   exact version to avoid any broken pipelines.
#   All available Hugo versions are listed under https://gitlab.com/pages/hugo/container_registry.
# - Read about the difference between hugo and hugo_extended
#   https://gitlab.com/pages/hugo/-/blob/main/README.md#hugo-vs-hugo_extended.
# - To change the theme, see
#   https://gitlab.com/pages/hugo/-/blob/main/README.md#use-a-custom-theme.
#
image: registry.gitlab.com/pages/hugo/hugo_extended:latest

# Set this if you intend to use Git submodules
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  HUGO_ENV: production

default:
  before_script:
  - apk add --no-cache go curl bash nodejs

test:
  script:
    - hugo
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  script:
    - hugo
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

### 3. Configure `.gitlab-ci.yml` in Your Repository

1. Ensure the `.gitlab-ci.yml` file is present at the root of your repository.
2. This configuration will:
   - Use the `hugo_extended` Docker image to build your static site.
   - Run the `hugo` command to generate the static files in the `public` directory.
   - Store the generated files as artifacts for deployment.
3. Modify the branch rules in the `test` and `pages` jobs as needed, based on your Git workflow.

### 4. Pushing Changes

Once you’ve configured your `.gitlab-ci.yml`, commit and push your changes:

```bash
git add .gitlab-ci.yml
git commit -m "Add GitLab Pages configuration"
git push origin main  # or the relevant branch
```

### 5. Pipeline Execution

When you push changes to the repository, GitLab CI/CD automatically triggers the pipeline:
- The `hugo` command will build your site.
- If successful, the site’s static files will be deployed to GitLab Pages.

You can monitor the pipeline status from the **CI/CD > Pipelines** section in your GitLab project.

### 6. Viewing the Site

After the pipeline succeeds, go to **Deploy > Pages** to access the webpage. For a website in the Gitlab of Huma-Num, the URL will take the following format:

> `https://<your-namespace>.gitpages.huma-num.fr/` 


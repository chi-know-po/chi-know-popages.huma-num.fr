---
title: "Corpora"
date: 2024-08-22
description: ""
keywords: []
weight: 1
---

The founding idea of the CHI-KNOW-PO project is that the fragmentation of the Chinese Medieval textual realm produced by modern disciplines is misleading to understand the intellectual background of Chinese literati. Hence, the project embraces a large body of texts belonging to belles lettres, texts of scholarship (dictionaries, encyclopedias and commentaries in particular), texts of technical knowledge (essays mainly).

This collection of texts will enable a diachronic study on how connections between specific words and word categories fared across the borders of genres, how these connections evolved, and how certain synonyms replaced the older terms to express a somewhat similar meaning.

The project will progressively share the results of its editorial work, which will result in XML-TEI files. The texts which will be edited include the following elements.

## Belles Lettres
*Wenxuan* 文選: Two versions of the anthology will be acquired based on two ancient books preserved at the BNU in Strasbourg - an 1809 edition with Li Shan's commentary, a 1923 edition with commentaries by the so-called Six ministers *liu chen* 六臣.

*Yutai xinyong* 玉臺新詠: A commented version of the anthology will be acquired based on a 1879 edition preserved at the IHEC in Paris (V XIV 69 (1-8)).

*Yuefu shiji* 樂府詩集: This anthology is being edited based on the Wikisource text.

*Quan Tang shi* 全唐詩: This anthology will be acquired based on a beautiful 1707 rare book preserved at the IHEC in Paris (SB 4002 (1-12) (1-120)).

## Scholarship
*Beitang shuchao* 北堂書鈔: The text of this early encyclopedia will be acquired base on a 1888 edition preserved at the BULAC in Paris (BIULO CHI.1087).

*Bowu zhi* 博物志: This text will be acquired based on a 1875 edition preserved at the BULAC in Paris (BIULO CHI.1140).

*Chuxue ji* 初學記: The text of this encyclopedia is based on a rare book preserved at the IHEC in Paris (SB 3701 (1-2)).

*Erya yintu* 影宋鈔繪圖爾雅: The text of this confucian classic will be acquired based on a 1801 illustrated edition of the Erya preserved at the BULAC in Paris (BIULO CHI.1938(1)-(3)).

*Mao Shi caomu niaoshou chongyu shu* 毛詩草木鳥獸蟲魚疏: This commentary of the Shijing will be acquired based on a 1857 edition preserved at the IHEC in Paris (V I 111 (1) 5).

*Mao Shi zhengyi* 毛詩正義: The Shijing confucian classic along with the four commentaries by Mao, Zheng Xuan, Kong Yingda and Lu Deming will be edited based on the Wikisource text.

*Shuowen jiezi* 說文解字: The text of this encyclopedia will be acquired based on a ca. 1900 edition preserved at the Institute of Chinese Studies of the University of Strasbourg.

*Yiwen leiju* 藝文類聚: The text of this encyclopedia will be acquired based on a 1879 edition preserved at the IHEC in Paris (CIII 5-7 (1-8)).

*Zhi bu zu zhai cong shu* 知不足齋叢書: Part of the text of this collection will be acquired based on a 1921 edition preserved at the IHEC in Paris (F X 2 (1-15) 1-120).

## Technical Knowledge
*Gujin shiwen leiju* 古今事文類聚: The text of this practical encyclopedia will be acquired based on a 1604 rare book preserved at the IHEC in Paris (SB 3705 (1-26)).

*Qimin yaoshu* 齊民要術: The text of the earliest agricultural treatise will be acquired based on a 1896 edition preserved at the IHEC in Paris (V I 22 (1-4)).

*Shennong bencao jing jizhu* 神農本草經集注: The text of this materia medica will be edited based on the Wikisource text with the help of other available edition including the Dunhuang manuscript.

*Xinzhai shizhong* 心齋十種: The text of the practical collection will be acquired based on a 1785/1788 edition of the collection preserved at the IHEC in Paris (V I 53 (1))
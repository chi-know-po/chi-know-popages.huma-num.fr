---
title: "Documentation information"
date: 2024-11-25
description: ""
keywords: []
weight: 3
---

We store metadata in the Nakala repository and in the header of each XML-TEI file.

In our example, we define language 1 as English, language 2 as Chinese and language 3 as French.

## Title
The title is mandatory. It can be expressed in mutliple languages.

We have chosen:
- to write it in *pinyin* transliteration with spaces between words (rather than characters), and with possible additional elements in English separated by ":" from the main title for language 1. Ex: Bowuzhi: roll 1
- to write it in Chinese characters (corresponding to the character style on the books used, generally traditional orthographic version), with possible additional elements separated by "：" from the main title for language 2. Ex: 博物志：卷一

This piece of information is used as nakala:title in the Nakala Repository.

We also document a short title in the yaml and the TEI files, in order to facilitate the reading when low-level titles are to be displayed on a screen along with the text below. This short title consists in the last part of the title (roll 1 / 卷一 in the example above).

## People
Among many choices, which are still under discussion by scientific editors of historical texts, we have decided:
- to put the historical author(s) of the text for reference in the Nakala Repository (nakala:creator).
- to spell their name in English/Pinyin (so that their name appears in alphabetical order when lists are displayed by the repository).

For the Bowuzhi, the author will appear as Hua Zhang. This is the only mandatory "author" in the metadata documentation. If their is no attribution to be stated, please choose "Anonymous". However, we need more information, especially in the case of a digital and multilingual research project.

We complement this piece of information in the Dublin Core vocabulary. For dcterms:creator, we add:
- a role (possibly in English, Chinese and French)
- the name in different scripts (latin corresponding to English, Chinese to Chinese) with the usual order (family name followed by first name).
- the dates of the author between parentheses.

We will hence indicate:
- Compiler : Zhang Hua (232-300) *in English*
- 張華 (232-300) 撰 *in Chinese*
- Compilateur : Zhang Hua (232-300) *in French*

In the TEI headers, the name is stated in Chinese and in *pinyin* transliteration. It is enriched with the corresponding unique identifier documented in the CHI-KNOW-PO database (tagged ckpp-).

The different roles attributed to authors, compilers, commentators, *etc* are documented in the CHI-KNOW-PO database (close list) and based on existing roles defined in Chinese books from the imperial era.

As for contemporary contributors, they are described as follows:
- the roles are modelled after the NISO [CRediT ontology](https://credit.niso.org/)
- names can be displayed in different languages, they are documented with: family name + first name + (ORCID:)

Although some contributors are not individuals but institutions and companies, they can also been added as contributors. Identifiers are always warmly recommended.

## Format
In the case of the CHI-KNOW-PO project, formats refer to both the format of the physical document and the format of the digital file.

### Document type
The Nakala repository demands we associate one single value to define the nakala:type of the digital objects. This type is documented by a url and defined by the 

In our project, we focus on three different types of documents:
- [books](https://vocabularies.coar-repositories.org/resource_types/c_2f33/): used for images of books
- [text](https://vocabularies.coar-repositories.org/resource_types/c_18cf/): used for editions
- [dataset](https://vocabularies.coar-repositories.org/resource_types/c_ddb1/): for databases and linked files such as documentation files and schemas.

Images of books are, in line with the coar descriptions of the document types, defined as books.

This information is not used outside Nakala in the CHI-KNOW-PO project.

### Digital format and standard
The CHI-KNOW-PO project adds information on the format in dcterms:format. This piece of information is identifiable in the name of the files. It can nevertheless be useful for users. It also explains that we attach, in the case of XML files, a standard: TEI.

### Other format related information
Finally, for images of historical books, we add information on the physical format of the original documents. This information is retrieved from catalogues of partner libraries.

## Keywords
There are two types of keywords.

We use expressions in multiple languages to document the files stored on Nakala, systematically documenting:
- the place (Chine)
- the period when the text was written
- the bibliographical category of the text (corresponding to the XXX table in the CHI-KNOW-PO database)
- 
Any additional element can be documented, it should always be translated in different languages.

We include the possibility of using keywords in the URI format to connect keywords to larger ontologies or controled vocabularies.

## Description
This paragraph is written in collaboration with our partner library. It serves to document the field dcterms:description. It is translated (sometimes in a shorter version) in multiple languages.

---
title: "Construction"
date: 2024-08-22
description: ""
keywords: []
weight: 2
---

## Lexical database sources

A structure of Chinese words and earliest text attestations of these words is taken over from _Computerlinguistische Datierung schriftsprachlicher chinesischer Texte_ [^1]. Information is parsed from a UTF-8 plain text version of _Hanyu da cidian_ 漢語大詞典 (_HDC_) [^2] and enriched with information from other sources, such as _CBDB_ [^3] and a corpus of _Early Chinese Texts_, as explained in "Loewe corpus" section in (Schalmey, 2021) [^4].

This lexical database was enriched with a main focus on specific semantic categories such as plants, emotions, naturel phenomena. To this end, different sources were parsed to document word categories, words within categories and equivalent/synonymic relationships, mainly for primary sources (based on wikisource versions of these texts):
- The _Chuxue ji_ 初學記 (for word categories and words in categories),
- The _Erya_ 爾雅 (for words in categories and synonymic relationships),
- The _Shuowen jiezi_ (for words in  categories)and synonymic relationships),
- The _Yiwen leiju_ (for word categories and words in categories).

This set of information was further enriched based on a digital index on medica materia, courtesy of Pr. Catherine Despeux [^5], which was automatically parsed.

The database has also been manually curated as a means to clean it and tidy it up. Cleaning is still ongoing.

##  Biographical and bibliographical information

A database of texts, comments and people of Chinese history relevant to the project has been started separately using Heurist [^6] and was then merged into the semantic database.

The reasoning behind this merging were the following ones:
1. The design of the Heurist database was too different from the architecture of CBDB which was taken as our model of inspiration;
2. The elements on time, people and texts were kinds of duplicates in both databases;
3. These elements were not only useful for editorial purposes: they were needed to provide the information about lexical occurrences;
4. Connection between two databases was not an easy process.

Time periods follow the ones used in CBDB with additional information on how some periods are linked to others (for example the Liang 梁 dynasty is part of the Six Dynasties 六朝 period). This is especially useful when information on a person or a text is not precise enough to situate it within one dynasty.

Information on people was manually documented based on databases or database crawlers (CBDB, ctext data wiki [^7], Academia Sinica [^8], IdRef [^9], VIAF [^10], dila [^11], biogref ([^12]) and on reference biographical dictionaries (especially Loewe [^13], Crespigny [^14], Knechtges and Chang [^15], Martin and Chaussende [^16]) completed by historical primary sources (mainly biographies in dynastic histories). All database and database crawler identifiers were recorded as a means to simplify identification. Priority was given to recording different names (family name, personal names, social names, posthumous names, etc) and dates of birth and death.

Information on texts was partly inherited from the lexical database. It was curated and enriched manually with titles of interest in the corpus. Titles and alternative titles and relations between texts (preface to, commentary of, included in, etc.), text and people (authored by, sent to, etc.), and text and time (produced/published in) were documented.

Some geographic information has been found in the texts and suggest it would be useful to document it in a separate table. This task has yet to be carried.

## References

[^1]: Schalmey Tilman. _Computerlinguistische Datierung schriftsprachlicher chinesischer Texte_. Diss., Universität Trier. Forthcoming (2022).

[^2]: Luo Zhufeng 羅竹風 (ed.). _Hanyu da cidian_ 漢語大詞典. Shanghai 上海: Cishu chubanshe 辭書出版社. 13 vols. 1986–1994.

[^3]: Fuller Michael A. (ed.), _China Biographical Database Project ([CBDB](https://projects.iq.harvard.edu/cbdb))_. 2017.

[^4]: Schalmey Tilman. “Raw frequency data: Thoughts on "Reliable" Learner's Vocabularies for Classical and Literary Chinese”. _Teaching Classical Chinese | Zum Unterricht des Klassischen Chinesischen | Wenyan wen jiaoxue_ 文言文教学. Ostasien Verlag. 2021: 251–261. (https://doi.org/10.5281/zenodo.5638881)

[^5]: Despeux Catherine (ed.). _Médecine, religion et société dans la Chine médiévale : Étude de manuscrits chinois de Dunhuang et de Turfan_. Paris: Collège de France. 3 vols. 2010.

[^6]: [Heurist](https://heurist.huma-num.fr/heurist/startup/)

[^7]: [ctext data wiki](https://ctext.org/datawiki.pl?if=en)

[^8]: [Academia sinica New archives](https://newarchive.ihp.sinica.edu.tw/sncaccgi/sncacFtp?@@0.8323215862251213)

[^9]: [Identifiants et référentiels pour l'enseignement supérieur et la recherche](https://www.idref.fr/)

[^10]: [Virtual International Authority File](https://viaf.org/)

[^11]: [Buddhist Studies Person Authority Databases](https://authority.dila.edu.tw/person/)

[^12]: [Biogref](https://biogref.org)

[^13]: Loewe Michael (ed.). _A biographical dictionary of the Qin, Former Han and Xin periods 221 BC - AD 24_. Leiden: Brill. 2000.

[^14]: de Crespigny Rafe (ed.). _A Biographical Dictionary of Later Han to the Three Kingdoms (23-220 AD)_. Leiden: Brill. 2007.

[^15]: Knechtges David and Chang Taiping (eds). _Ancient and Early Medieval Chinese literature : A Reference Guide_. Leiden: Brill. 4 vols. 2010-2014.

[^16]: Martin François and Chaussende Damien (eds). _Dictionnaire biographique du haut Moyen âge chinois : culture, politique et religion de la fin des Han à la veille des Tang, IIIe-VIe siècles_. Paris: Belles Lettres. 2020.

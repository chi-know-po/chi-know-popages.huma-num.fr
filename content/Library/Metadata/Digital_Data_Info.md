---
title: "Informations of use for digital purposes"
date: 2024-11-25
description: ""
keywords: []
weight: 4
---
## License
We use two types of licenses.

Creative Commons licenses are used for images and texts:
- CC-BY-NC-SA-4.0 for all images and for texts whose edition is based on images as well as for database
- CC-BY-SA-4.0 for all texts whose edition is base on Wikisource texts.

MIT licenses are used for scripts.

## Dates
The situation for dates is complex, since we distinguish:
- date of publication of the text in a book (Nakala:created): this date can be expressed in a formated way (YYYY), by a an approximation (ca. YYYY or YYYY-YYYY) or by an expression in a traditional calendar. This last for should be used for the TEI header only. It is recommended to used the formated way or the approximation in the Nakala repository. This pieced of information is mandatory.
- date of creation of the original text (dcterms:created): this date can be expressed in a formated way (YYYY), by a an approximation (ca. YYYY or YYYY-YYYY).
- date of online publication by the CHI-KNOW-PO project (dcterms:available): here we indicate when the digital files were published. Today's rich formated should be preferred (YYYY-MM-DD)
- date of modification of the files (dcterms:modified): to be documented only if a new version of the files were uploaded.

## Status
The objective is to share all scientific products of the CHI-KNOw-PO project in the Nakala repository under the "published" status with no embargo.

It is however recommended to first send all text files along with metadata under the "pending" status. Once all elements are sent on the Nakala repository, it is possible to retrieve the DOIs that we need to integrate in the header of all text files.

## Versions
We use the recommendations of the TEI consortium to change the status of the text files edited in the context of the CHI-KNOW-PO project.
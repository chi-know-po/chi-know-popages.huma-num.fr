# CHI-KNOW-PO/Library

## Installation

1. Create conda environment and activate it:
```bash
conda create --name chi-know-po-documentation python=3.12 -y && \
conda activate chi-know-po-documentation
```

2. Install [Poetry](https://python-poetry.org/) and project dependencies:
```bash
conda install -c conda-forge poetry -y && \
poetry install
```

3. Import submodule (*ace-documentation theme*)
```bash
git submodule init
git submodule update
```

## Contribute

To contribute, please follow the documentation on the `Self-documentation/Contribute` page. The following outlines the essentials:

1. Run CHI-KNOW-PO locally  
   
   1.1. Edit `config.toml` by commenting the first line and uncommenting the second:  

    <u>Production:</u>
    ```markdown
    baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
    # baseURL = "http://localhost:1313/" # for local testing
    ```
    <u>Testing:</u>
    ```markdown
    # baseURL = "https://chi-know-po.gitpages.huma-num.fr/" # for production
    baseURL = "http://localhost:1313/" # for local testing
    ```

   1.2. Run the server:
    ```bash
    hugo server
    ```

    1.3. Access the website locally ([http://localhost:1313/](http://localhost:1313/)) and any change you make will be displayed.  

2. Push your changes to the remote repository:
```bash
git push origin main
```

This will trigger the CI/CD pipeline and deploy the changes to [the website](https://chi-know-po.gitpages.huma-num.fr/). automatically.

**/!\\** Please note that when pushing your changes, **you should not include the `config.toml` file with the testing parameters URL**. If you need to make config changes, make sure to comment out the testing URL and uncomment the production URL before pushing your changes.

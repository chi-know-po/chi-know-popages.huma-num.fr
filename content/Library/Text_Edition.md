---
title: "Text Edition"
date: 2024-10-11
description: "An overview of XML Edition steps in the CHI-KNOW-PO project, focusing on transforming ALTO files into TEI-compliant documents."
keywords: []
weight: 3
---

This section provides an in-depth guide on how to edit XML files for the CHI-KNOW-PO project, specifically transforming **Analyzed Layout and Text Object (ALTO)** files into fully compliant **Text Encoding Initiative (TEI)** documents.

### Overview of the XML Edition Workflow

| **Step** | **Description** | **Details** |
|:--------:|:---------------:|:-----------:|
| 1    | ALTO to CSV Conversion         | Extract textual data from ALTO files and convert to CSV for manual verification.     |
| 2    | Aggregate Metadata             | Collect and organize metadata like chapter titles, authorship, and content details.  |
| 3    | Create Table of Contents (TOC) | Establish document structure by organizing sections and chapters.                    |
| 4    | Extract Book Structure         | Define chapters and subsections to represent logical text organization.              |
| 5    | Align Text and Structure       | Align text within the hierarchical format to maintain consistency across the corpus. |
| 6    | Generate TEI Files             | Convert structured CSV into TEI-compliant XML for research and archiving.            |


The XML Edition process involves converting ALTO files into TEI files using a structured pipeline. The process is divided into **six main steps**, each crucial for ensuring that the textual content is properly transformed, annotated, and made suitable for scholarly use. Below, each step is explained not only in terms of what is done, but also why it is necessary for the XML edition process.

## Step-by-Step Pipeline

The Edition pipeline is located in the folder `src/Edition/` inside the [**Library**](https://gitlab.huma-num.fr/chi-know-po/library) repository.

> **Note**: Replace "`TITLE`" in the paths with the specific corpus name you are working with. For example, for the LJ Shu corpus, the correct paths are `../../data/LJ_Shu`.


1. <span style="color:red">Complete the HTML form in `src/Edition/yaml_form` by documenting all the metadata information.</span>
   
   ![HTML form for metadata](/images/html_form.png)

2. **Convert ALTO to CSV for Manual Verification**

   - **Purpose**: The first step in the pipeline is to extract textual data from ALTO XML files and convert it into a CSV format. This conversion is important because ALTO files often contain scanned text with potential recognition errors that need manual correction.
   - **Notebook**: `1_Alto2CSV_to_check.ipynb`
   - **Input**: XML files in `../../data/TITLE/Altos/`
   - **Output**: `../../data/TITLE/Edition/TITLE_to_check.csv`
   - **Why**: Converting to CSV makes it easier for human editors to manually verify and correct textual inaccuracies, ensuring the integrity of the text before further processing.

3. <span style="color:red">Check manually `to_check.csv` and save it as `checked.csv`.</span>

   - Manual verification here is crucial for enhancing accuracy, especially in texts derived from optical character recognition (OCR).  
   
4. **Aggregate Metadata**

   - **Purpose**: This step involves collecting and organizing metadata that describes the structure and content of the text. Metadata includes information such as chapter titles, authorship, publication dates, and other contextual details.
   - **Notebook**: `2_Create_Metadata_csv.ipynb`
   - **Input**:
     - `../../data/TITLE/TITLE.yaml` (YAML metadata file)
     - `../../data/TITLE/Altos/`
   - **Output**: `../../data/TITLE/Edition/TITLE_metadata.csv`
   - **Why**: Metadata aggregation is essential for generating structured TEI documents. Metadata provides contextual information that enhances the discoverability, usability, and scholarly value of the texts.

5. <span style="color:red">Check manually `metadata.csv` and save it as `metadata_ok.csv`.</span>

6. **Create Table of Contents (TOC)**

   - **Purpose**: Creating a table of contents provides a structural overview of the document, helping to establish clear divisions between different sections or chapters of the text.
   - **Notebook**: `3_Create_TOC.ipynb`
   - **Input**:
     - `../../data/TITLE/TITLE_checked.csv` or `../../data/TITLE/TITLE_to_check.csv`
     - `../../data/TITLE/TITLE.yaml`
   - **Output**: `../../data/TITLE/Edition/TITLE_toc.csv`
   - **Why**: The TOC is crucial for establishing the internal organization of the text, which is fundamental for both navigation and scholarly analysis. It ensures that the document structure is accurately represented in the final TEI format.

7. <span style="color:red">Check manually `toc.csv` and save it as `toc_ok.csv`.</span>

8. **Extract Book Structure**

   - **Purpose**: This step extracts the hierarchical structure of the book, including chapters and sub-sections, to create a representation that can be used to organize the TEI files.
   - **Notebook**: `4_Create_Structure_csv.ipynb`
   - **Input**:
     - `../../data/TITLE/TITLE_toc.csv` or `../../data/TITLE/TITLE_toc_ok.csv`
   - **Output**: `../../data/TITLE/Edition/TITLE_structure.csv`
   - **Why**: Extracting the structure is essential for ensuring that the TEI files reflect the logical and semantic organization of the original work. This step helps maintain a consistent structure across the entire corpus, which is vital for accurate representation and analysis.

9.  **Align Text and Structure**

   - **Purpose**: Aligning the text with the extracted structure ensures that each segment of the text is correctly placed within the overall hierarchy defined in the TOC and metadata.
   - **Notebook**: `5_Concat2Numbered_csv.ipynb`
   - **Input**:
     - `../../data/TITLE/TITLE_structure.csv`
     - `../../data/TITLE/TITLE_checked.csv` or `../../data/TITLE/TITLE_to_check.csv`
     - `../../data/TITLE/TITLE_metadata_ok.csv` or `../../data/TITLE/TITLE_metadata.csv`
   - **Output**: `../../data/TITLE/TITLE_numbered.csv`
   - **Why**: Proper alignment is necessary to integrate the text within the predefined structure, making sure that all sections and chapters are numbered and organized systematically. This step is critical for maintaining the coherence and readability of the final TEI output.

10. **Generate TEI Files**

   - **Purpose**: The final step involves converting the structured CSV into TEI-compliant XML files, which are suitable for digital humanities research and archiving.
   - **Notebook**: `6_Generate_TEI.ipynb`
   - **Input**:
     - `../../data/TITLE/TITLE_numbered.csv`
     - `../../data/TITLE/TITLE_metadata.yaml`
   - **Output**: All XML files are saved in `../../data/TITLE/TEI/` directory.
   - **Why**: TEI (Text Encoding Initiative) is a standard for representing texts in digital form, which allows for rich annotation and interoperability across different systems. This step ensures that the text is encoded in a format that is suitable for both human and machine reading, facilitating research and analysis.

## Installation

Before starting with the XML Edition workflow, you need to clone the CHI-KNOW-PO [**Library**](https://gitlab.huma-num.fr/chi-know-po/library) project to your local machine. You can do this by running the following command:

```bash
git clone https://gitlab.huma-num.fr/chi-know-po/library.git
```

The repository contains all the necessary files and scripts required for digitizing, editing, and publishing a corpus of ALTO files.

1. **Create Conda Environment and Activate It**:

   ```bash
   conda create --name chi-know-po-library python=3.12 -y && \
   conda activate chi-know-po-library
   ```

2. **Install Poetry and Project Dependencies**:

   ```bash
   conda install -c conda-forge poetry -y && \
   poetry install
   ```

3. **Set Python Path to Source Folder**:

   ```bash
   export PYTHONPATH=$(pwd)/src
   ```

These steps will set up the necessary environment and dependencies for running the XML Edition pipeline.

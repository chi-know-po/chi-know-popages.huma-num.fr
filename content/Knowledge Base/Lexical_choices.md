---
title: "Lexical choices"
date: 2024-08-22
description: ""
keywords: []
weight: 3
---

## On so-called synonymic/equivalent relationships

Note that these correspondances do not exactly cover what we would call synonyms. These correspondances state that one word is equivalent to another word without any information of the time the two words were used with this specific shared meaning.

As a consequence, our synonymic chains of words produced by the database are theoretical ones, established in different texts, but where:
1. A = B may in a number cases mean that the word A in texts from the Antiquity carries the same meaning as the one carried by word B in present time; 
2. A = B and A = C produce B = C although it is not stated anywhere (and might not be true outside of the texts, meaning that a plant B might be considered equivalent to plant C to write a poem, but not to cure someone with a specific disease).

## Synonym curation

The lexical part of the database establishes links between words (synonyms, although more likely equivalences). These links were largely produced automatically by scraping through ancient dictionaries. However, works such as the *Erya* 爾雅 state many equivalences that would prove misleading when used to further analyze texts and identify blurry text-reuse, because some words are almost never used under this obsolete meaning. We hence curate manually the database to deactivate such links.

## Which semantic fields?

Words are also linked to semantic fields to which they belong.

Because the purpose of this part of the project was to facilitate our understanding of how words belonging to different semantic fields cooccurred in prose and poetry, the CHI-KNOW-PO team had to choose and map down categories corresponding to semantic fields. Hence, the question was: On which criteria should these categories be established?

Rather than pick categories based on one's own subjective perception of language, it has been decided to rely on categories existing within the books under study.

## Classification model

The classification into groups is thus modeled after traditional Chinese encyclopedias (e.g. _Chu xue ji_ 初學記, _Yiwen Leiju_ 藝文類聚). Not all categories of all early encyclopedias have been included: a selection has been made in relation with the poetic corpus under study, and some correspondences between encyclopedias have been established.

This choice of encyclopedias over dictionaries is fundamental as it acknowledges that words with a shared radical do not necessarily belong to the same semantic field. Nonetheless, dictionaries played an important role in the enrichment of the lexical part of the database, especially since dictionaries establish correspondances between words.

## Semantic categories usage in the scripts

The collocation explorer section of the notebook allows one to search for cooccurrence of two semantic categories called "concept1" and "concept2". These can be large categories or smaller categories.

Categories are called by their English translation.

It is possible to choose smaller categories and to check the source from which the category was pulled.

For this, please see the category_tree.ipynb notebook.

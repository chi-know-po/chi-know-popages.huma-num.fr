---
title: "Text Acquisition"
date: 2024-08-22
description: ""
keywords: []
weight: 2
---
Text acquisition is based on two main pipelines:
1. either reuse - with additional editorial work - of digital open source material,
2. either digitization of ancient books, followed by HTR full text extraction and editorial follow-up.

## Text Edition based on Digital open source material

Two repositories offer precious open source resources for the study of ancient texts, namely, Wikisource and Kanripo. Those are the resources we rely on for further editorial work.

This work is needed because of a lack of neat structure of the texts provided by both repositories.

Because of the complex structure of most of the texts under study, which very often assemble texts, titles, authors and exegetical apparatus sometimes by different authors of distinct periods, we have decided to edit our entire body of texts in order to reveal this complex structure and identify each part of the text. To reach this aim, we convert the html or txt sources in xml and follow the Text Encoding Initiative (TEI) guidelines.

Our first edition of the type was the [*Shijing* 詩經 edition](https://gitlab.huma-num.fr/chi-know-po/shijing-net) whose result will finally appear in January 
2023.

We are presently working on the edition of the *Yuefu shiji* 樂府詩集, which is characterize by more automatic treatment, less attention to details such as character variants, and whose result should appear in March 2023. 

## Digitization and full text extraction

### Process

Most of the body of texts that will be acquired by the CHi-KNOW-PO team was:
1. digitized as pictures of ancient books preserved in French libraries (and published as such);
2. transformed into full text thanks to HTR;
3. converted under XML-TEI format.

This text acquisition is made possible thanks to the collaboration with four libraries, our annotation (page layout and text) and the collaboration with Calfa.

The Calfa team has developed a specific architecture based on the HTR engine Kraken which allows us to train models based on very limited annotation work. A new approach to page layout will be developed in the process of this collaboration in order to more efficiently grasp the specificities of ancient Chinese page layout (with large columns for main text and double columns of smaller characters for gloses for instance.

### Layout Analysis


### Annotation


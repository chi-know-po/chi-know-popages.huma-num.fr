---
title: "Knowledge Base"
date: 2024-08-22
description: ""
keywords: []
weight: 2
---

## Design principles

The CKP database is designed as a relational (my)SQL database, using unique numeric IDs as object identifiers. We strive towards using these IDs when the object is referenced, although some exceptions may be made.

## Database general description

The database is structured around words and their use. As such, it contains a table of words, indications to which group they belong to (e.g. plants, emotions), what their synonyms are and the first known source to use the word. It contains a large number of other data related to this core: information about the source texts, their authors and commentators _etc_.

In addition to words, semantic categories and synonyms, the database holds information on persons of Chinese history, literary works and connections between these objects. This part is linked to words (as we record first identified occurrence of words in texts by authors). It is nonetheless the result of a specific development (see [Database construction](/database construction)).

The CKP database consists of several main objects that are connected via unique identifiers.

## Database use

The database is used mainly for retrieving information on synonyms, semantic categories and words and text segmentation.

In this version, the database is used for:

1. getting a dictionary for text tokenization
2. getting a list of words belonging to a concept (see Scripts – looking for collocations between concepts)
3. getting a list of synonyms to a given word (see Scripts – looking for all the pairs similar to a chosen one)

In parallel, the database is used by the CHI-KNOW-PO team to tag the large body of text that is being edited in XML-TEI format.

Some functions of the CHI-KNOW-PO (CKP) scripts rely on an underlying mySQL relational database. 

A version of the database with a time stamp is available in the database folder for reuse.

## Further information

For more detailed information, see:

[Database construction](/database-construction)

[Database description](/database-description)

[Semantic classification](/semantic-classification)

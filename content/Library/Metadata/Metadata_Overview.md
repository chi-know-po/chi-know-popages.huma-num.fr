---
title: "Metadata overview"
date: 2024-12-02
description: ""
keywords: []
weight: 5
---
Abbreviations:
- Y: Yes
- N: No
- NA: Not Applicable
- R: Recommended

Formats include:
- string (series of characters)
- NAME;Surname (two series of characters separated by semi-column)
- URI (identifier consisting in a series of characters meant to locate a resource on a computer or on the Web)
- YYYY (year in four digits)
- MM (month in two digits)
- DD (day in two digits)

## Metadata Items
### People

|  Column | Reference Term  | Format | Language |Nakala Required| TEI Required | Sep. if repeatable |
|---|---|---|---|---|---|---|
| author | nakala:creator   | NAME;Surname | English/Pinyin | Y  | Y | NA |
| creators  |  dcterms:creator |  NameSurname (dates) | Chinese | R  | R | NA  |
| contributors | dcterms:contributor  |  Role - Role: Name Surname (ORCID) | All  | R  | R  | &#124;  |

### Title
|  Column | Reference Term  | Format | Language |Nakala Required| TEI Required | Sep. if repeatable |
|---|---|---|---|---|---|---|
| title1 | nakala:title | string | Language1 | Y | Y | NA |
| title 2 | nakala:title  | string |  Language2 | R | Y | NA |

### Dates
|  Column | Reference Term  | Format | Language | Nakala Required| TEI Required |  Sep. if repeatable |
|---|---|---|---|---|---|---|
| date | nakala:created | YYYY | NA | Y  | Y  | NA  |
| created | dcterms:created  | YYYY or ca. YYYY or YYYY-YYYY |  NA |  R | R | NA |
| available  | dcterms:available  | YYYY-MM-DD | NA  | R  | Y  | NA |
| modified  |  dcterms:modified | YYYY-MM-DD  |  NA | R  | R  | NA |

### Other Data
|  Column | Reference Term  | Format | Language |Nakala Required| TEI Required | Sep. if repeatable |
|---|---|---|---|---|---|---|
| file |   | filename.format |   |  Y |  | &#124; |
| embargo  |   |  YYYY or embargo |  NA | N  | NA | NA |
| status  |   |   |   |  Y |   |  NA |
| datatype  | nakala:type  | URI | IR |  Y | NA | NA |
| licence | nakala:license  |   |   |   |   |   |
| descriptionMain |   |   |   |   |   |   |
| descriptionSec  |   |   |   |   |   |   |
| keywordsZh  |   |   |   |   |   | &#124;  |
| keywordsEn |   |   |   |   |   |  &#124; |
| keywordsURI |   |   |   |   |   | &#124;  |
| language  |   |   |   |   |   |   |
| publisher  | dcterms:publisher |   | NA  | R  | R  | &#124;  |
| format |   | string | All  | R  | R | ; |
| conformsTo |   |   |   |   |   | &#124;  |
| version |   |   |   |   |   |   |
| collectionIDs |   |   |   |   |   | &#124;  |
| rights |   |   |   |   |   | &#124;  |

## Standard vocabularies

### Datatype (nakala:type)
We select between three types:
- [books](https://vocabularies.coar-repositories.org/resource_types/c_2f33/): used for images of books
- [text](https://vocabularies.coar-repositories.org/resource_types/c_18cf/): used for editions
- [dataset](https://vocabularies.coar-repositories.org/resource_types/c_ddb1/): for databases and linked files such as documentation files and schemas.
Please read documention to choose other types within the [COAR standard vocabulary] (https://vocabularies.coar-repositories.org/resource_types/).

### Status
- pending: before publication
- published: for publication

### Languages
Use codes established by [ISO 639.3](https://iso639-3.sil.org/)

### People related identifiers
#### Individual and institution identifiers
#### Roles

### Keywords URI

## Methods
To retrieve identifiers from the Nakala repository, one can:
- use the GET method (for groupes and collection IDs) of the online [Nakala API connection tool] (https://api.nakala.fr/doc)
- use the notebooks available in the [Library repository](https://gitlab.huma-num.fr/chi-know-po/library/-/tree/main/src/MetadataToRepository) of the CHI-KNOW-PO project